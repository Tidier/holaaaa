/**
 * 
 */
package Arrays;

import javax.swing.JOptionPane;

/**
 * @author Administrador
 *
 */
public class Ejercicio4 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		
		String texto=JOptionPane.showInputDialog("Introduce un numero");
        int numero=Integer.parseInt(texto);
		JOptionPane.showMessageDialog(null,"Los factores de "+numero+" son "+factores(numero));
	}
		
		public static int factores (int numero){
			int res=numero;
			for(int cont=(numero-1);cont>1;cont--){
				res=res*cont;
			}
        
		return res;	
	}

}
