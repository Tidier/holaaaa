/**
 * 
 */
package Math.copy;

import javax.swing.JOptionPane;

/**
 * @author Administrador
 *
 */
public class Ejercicio5 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String num=JOptionPane.showInputDialog("Introduce numero");
		
		double num_new=Double.parseDouble(num);
		
		if (num_new%2==0){
			JOptionPane.showMessageDialog(null,"El numero es divisible entre 2");
			}
		else{
			JOptionPane.showMessageDialog(null,"El numero no es divisible entre 2");
		}
	}

}
