package Math.copy;

import javax.swing.JOptionPane;
public class Ejercicio4 {

	public static void main(String[] args) {
		
		final double PI=3.1416;
		String radio=JOptionPane.showInputDialog("Introduce radio de la circunferencia");
		
		double radio_new=Double.parseDouble(radio);
		
		double resultado= PI*(Math.pow(radio_new, 2));
		JOptionPane.showMessageDialog(null,"El area es "+resultado);

	}

}
